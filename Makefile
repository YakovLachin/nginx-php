create-network:
	@-docker network create simple-net

up: create-network
	@docker-compose up

.DEFAULT_GOAL := up
